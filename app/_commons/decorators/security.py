from django.http import HttpResponseRedirect, Http404
from django.core.exceptions import PermissionDenied

from _commons.security.defaults import auth_manager, reg_manager, membership_manager


def auth_required(f):
    """
    Protects auth-required pages
    """
    def wrap(request, *args, **kwargs):
        return auth_manager(request) or f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def anon_required(f):
    """
    Assert anonymous-required pages
    """
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect('/')
        return f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def reg_required(f):
    """
    Protects registration pages (semi-auth)
    """
    def wrap(request, *args, **kwargs):
        return reg_manager(request) or f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def new_required(f):
    """
    Protects new registration page (semi-auth)
    """
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated():
            return reg_manager(request) or f(request, *args, **kwargs)
        else:
            return f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def member_required(f):
    """
    Protects Diapason pages (if Diapason requires membership)
    """
    def wrap(request, *args, **kwargs):
        membership = membership_manager(request, kwargs.get('diapason_name'))

        if request.user.is_authenticated() and membership['is_allowed'] is True:
            return f(request, *args, **kwargs)

        if membership['isnt_found'] is True:
            raise Http404

        raise PermissionDenied

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap
