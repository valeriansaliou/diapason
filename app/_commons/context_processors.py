from django.conf import settings


def conf(request):
    """
    Return the settings in context
    """
    return {
        'conf': settings,
    }
