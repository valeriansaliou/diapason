# -*- coding: utf-8 -*-

from django import forms
from _commons.forms import defaults
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from _commons.validators import _is_valid
from _commons.forms.errors import field_error

from .models import *


# -- Helper: validate after stripping value
def _validate_strip(value):
    if not value or not value.strip():
        raise ValidationError(None, 'required')


# -- Diapason: New
class DiapasonNewForm(forms.Form):
    """
    Diapason > Diapason New Form
    """

    name = forms.CharField(
        required=True,

        widget=forms.TextInput(
            attrs={
                'class': 'name',
                'placeholder': 'Entrez le nom du Diapason...'

            }
        ),

        error_messages={
            'required': 'Entrez le nom du Diapason.',
            'invalid': 'Le nom du Diapason est invalide ou déjà utilisé.'
        }
    )

    description = forms.CharField(
        required=True,

        widget=forms.Textarea(
            attrs={
                'cols': 42,
                'rows': 20,
                'class': 'description',
                'placeholder': 'Décrivez ce Diapason...'
            }
        ),

        error_messages={
            'required': 'Entrez une description.',
            'invalid': 'La description est invalide.'
        }
    )

    visibility_level = forms.TypedChoiceField(
        choices=VisibilityLevels.choices(),
        coerce=int,

        widget=forms.HiddenInput(
            attrs={
                'class': 'visibility_level'

            }
        ),

        error_messages={
            'required': 'Sélectionnez une visibilité.',
            'invalid': 'La visibilité est invalide.'
        }
    )

    proposals_per_cycle = forms.TypedChoiceField(
        choices=ProposalsPerCycle.choices(),
        coerce=int,

        error_messages={
            'required': 'Sélectionnez un nombre de propositions.',
            'invalid': 'Le nombre de propositions est invalide.'
        }
    )

    def is_valid(self):
        return _is_valid(self, forms.Form)
