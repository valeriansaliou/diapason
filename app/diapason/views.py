# -*- coding: utf-8 -*-

import uuid
import datetime
import re

from django.core.mail import send_mail
from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.models import User

from _commons.decorators.security import auth_required, member_required
from django.conf import settings

from .forms import *
from .helpers import *
from .models import *

from account.models import Profile

import logging
logger = logging.getLogger(__name__)


def invite(request):
    """
    Diapason > Invite
    """
    key_is_valid = False
    key = request.GET.get('key', '').strip()

    # Verify key
    if DiapasonInvite.objects.filter(secret=key).count() > 0:
        key_is_valid = True

    if key_is_valid == True:
        if request.user.is_authenticated():
            # Accept immediately
            diapason_accepted = InviteHelper.check_and_accept_invite(request)

            # Redirect user to diapason
            if diapason_accepted != None:
                return HttpResponseRedirect("/diapason/assembly/%s/" % diapason_accepted.slug)

    return render(request, 'diapason/diapason_invite.jade', {
        'key_is_valid': key_is_valid,
        'key': key
    })


@auth_required
def new(request):
    """
    Diapason > New
    """
    error_trace = None
    commits_pending = []

    if request.method == 'POST':
        form = DiapasonNewForm(request.POST)

        if form.is_valid():
            slug = str(uuid.uuid4())
            name = form.cleaned_data['name']
            description = form.cleaned_data['description']
            visibility_level = form.cleaned_data['visibility_level']
            proposals_per_cycle = form.cleaned_data['proposals_per_cycle']

            members = request.POST.get('member_ids', '').split('|')

            # Insert Diapason
            if Diapason.objects.filter(name=name).count():
                error_trace = u"Un diapason avec le même nom existe déjà"
            else:
                # Create Diapason
                diapason = Diapason(
                    slug=slug,
                    name=name,
                    description=description,
                    visibility_level=visibility_level,
                    proposals_per_cycle=proposals_per_cycle,
                )

                commits_pending.append(diapason)

                # Create membership
                member = DiapasonMember(
                    diapason=diapason,
                    user=request.user,
                    level=1
                )

                commits_pending.append(member)

                # Add members to Diapason
                for member_uid in members:
                    if member_uid and request.user.username != member_uid:
                        logger.info('Proceed invites...')

                        # Email?
                        try:
                            # Fails if not found.
                            member_uid.index("@")

                            logger.info('Email invite')

                            # Create invite secret key
                            secret = str(uuid.uuid4())

                            # Insert Diapason invite
                            invite = DiapasonInvite(
                                diapason=diapason,
                                secret=secret
                            )

                            commits_pending.append(invite)

                            # Send invite email
                            send_mail(
                                u"Invitation à rejoindre un Diapason",
                                (u"Bonjour,\n\nVous avez été invité à rejoindre un groupe de débats sur Diapason.\n Accédez au lien d'invitation suivant : %sdiapason/invite/?key=%s\n\n--\n\nDiapason." % (settings.SITE_URL, secret)),
                                "noreply@diapason.org",
                                [member_uid],
                                fail_silently=False,
                            )
                        except ValueError:
                            logger.info('User invite')

                            # MID
                            member_user = User.objects.get(username=member_uid)

                            ext_member = DiapasonMember(
                                diapason=diapason,
                                user=member_user,
                                level=0
                            )

                            commits_pending.append(ext_member)

                # Create cycles
                cycle_i = 1

                cycle_defined = request.POST.get("cycle_%s_defined" % cycle_i, "0")
                cycle_name = request.POST.get("cycle_%s_name" % cycle_i, None)
                cycle_topic = request.POST.get("cycle_%s_topic" % cycle_i, None)
                cycle_tags = request.POST.get("cycle_%s_tags" % cycle_i, None)
                cycle_autoclose = request.POST.get("cycle_%s_autoclose" % cycle_i, None)

                while cycle_defined == "1":
                    if cycle_name and cycle_topic and cycle_tags and cycle_autoclose:
                        try:
                            # Parse autoclose date
                            autoclose_date = datetime.strptime(cycle_autoclose, "%d/%m/%Y").date()

                            cycle = DiapasonSession(
                                author=request.user,
                                diapason=diapason,
                                name=cycle_name,
                                topic=cycle_topic,
                                tags=cycle_tags,
                                status=0,
                                date_autoclose=autoclose_date
                            )

                            commits_pending.append(cycle)

                            cycle_i += 1

                            cycle_defined = request.POST.get("cycle_%s_defined" % cycle_i, "0")
                            cycle_name = request.POST.get("cycle_%s_name" % cycle_i, None)
                            cycle_topic = request.POST.get("cycle_%s_topic" % cycle_i, None)
                            cycle_autoclose = request.POST.get("cycle_%s_autoclose" % cycle_i, None)
                        except Exception as e:
                            error_trace = (u"La date de clôture est invalide pour le cycle no. %s car: %s" % (cycle_i, str(e)))
                            break
                    else:
                        error_trace = (u"Le cycle no. %s n'est pas complètement défini" % cycle_i)
                        break

            # Redirect to Diapason
            if error_trace is None:
                # Commit all saves
                for commit_atom in commits_pending:
                    commit_atom.save()

                return HttpResponseRedirect("/diapason/assembly/%s/" % slug)
    else:
      form = DiapasonNewForm()

    return render(request, 'diapason/diapason_new.jade', {
        'form': form,
        'error_trace': error_trace
    })


@auth_required
def new_user_search(request, query):
    """
    Diapason > New / User / Search
    """
    results = User.objects.filter(first_name__startswith=query)
    data = serializers.serialize('json', results)

    return HttpResponse(data, content_type='application/json')


@auth_required
@member_required
def assembly(request, diapason_name):
    """
    Diapason > Assembly
    """
    response_data = AssemblyHelper.assembly_build_response(request, diapason_name)

    if response_data['status'] == 'not_found':
        raise Http404

    # Submit proposal?
    if request.method == 'POST' and response_data['can_participate'] is True:
        proposal_title = request.POST.get('proposal_title', None)
        delegate_subject = request.POST.get('delegate_subject', None)
        delegate_member_id = request.POST.get('delegate_member_id', None)
        undelegate_subject = request.POST.get('undelegate_subject', None)
        undelegate_member_id = request.POST.get('undelegate_member_id', None)
        member_id = request.POST.get('member_id', None)
        service_type = request.POST.get('service_type', None)
        service_uri = request.POST.get('service_uri', None)

        if undelegate_subject and undelegate_member_id:
            undelegate_user = User.objects.get(username=undelegate_member_id)

            DiapasonDelegation.objects.filter(user=request.user, delegated_user=undelegate_user, subject=undelegate_subject, diapason=response_data['diapason']).delete()

            # Refresh response data
            response_data = AssemblyHelper.assembly_build_response(request, diapason_name)

        if delegate_member_id and delegate_subject:
            delegate_user = User.objects.get(username=delegate_member_id)

            DiapasonDelegation.objects.get_or_create(user=request.user, delegated_user=delegate_user, diapason=response_data['diapason'], subject=delegate_subject)

            # Refresh response data
            response_data = AssemblyHelper.assembly_build_response(request, diapason_name)

        if proposal_title is not None:
            if proposal_title:
                proposal_already_exists = False
                proposal_overflow = False

                # Build fuzzy match regex
                match_term = ".*"

                for term in proposal_title.split(" "):
                    match_term = match_term + ("(?=.*" + term + ".*)")

                match_term = match_term + ".*"

                proposal_name_regex = re.compile(match_term.upper())

                # Proceed text mining on already existing proposals
                existing_proposals = DiapasonProposal.objects.filter(diapason_session__in=response_data['all_sessions'])

                for proposal in existing_proposals:
                    if proposal.name and proposal_name_regex.match(proposal.name.upper()):
                        proposal_already_exists = True

                        response_data['alert_response_data_error'] = "La proposition semble déjà exister, et n'a donc pas été re-créée."

                # Count existing proposals
                if response_data['diapason'].proposals_per_cycle < 4 and (DiapasonProposal.objects.filter(diapason_session=response_data['active_session']).count() >= (response_data['diapason'].proposals_per_cycle + 1)):
                    proposal_overflow = True

                    response_data['alert_response_data_error'] = "Il y a trop de propositions à cette session. Attendez la prochaine session."

                # Insert proposal?
                if proposal_already_exists is False and proposal_overflow is False:
                    DiapasonProposal.objects.get_or_create(author=request.user, diapason_session=response_data['active_session'], name=proposal_title)

                    try:
                        response_data['alert_response_data_success'] = True

                        # Refresh response data
                        response_data = AssemblyHelper.assembly_build_response(request, diapason_name)
                    except:
                        response_data['alert_response_data_error'] = "La proposition n'a pas pu être créée. Réessayez."
            else:
                response_data['alert_response_data_error'] = "La proposition n'a pas pu être créée. Réessayez."

        if member_id is not None:
            if member_id and request.user.username != member_id:
                logger.info('Proceed invites...')

                # Email?
                try:
                    # Fails if not found.
                    member_id.index("@")

                    logger.info('Email invite')

                    # Create invite secret key
                    secret = str(uuid.uuid4())

                    # Insert Diapason invite
                    invite = DiapasonInvite(
                        diapason=response_data['diapason'],
                        secret=secret
                    )

                    invite.save()

                    # Send invite email
                    send_mail(
                        u"Invitation à rejoindre un Diapason",
                        (u"Bonjour,\n\nVous avez été invité à rejoindre un groupe de débats sur Diapason.\n Accédez au lien d'invitation suivant : %sdiapason/invite/?key=%s\n\n--\n\nDiapason." % (settings.SITE_URL, secret)),
                        "noreply@diapason.org",
                        [member_id],
                        fail_silently=False,
                    )
                except ValueError:
                    logger.info('User invite')

                    # MID
                    member_user = User.objects.get(username=member_id)
                    DiapasonMember.objects.get_or_create(diapason=response_data['diapason'], user=member_user, level=0)
            else:
                response_data['alert_response_data_error'] = "Le membre n'a pu être invité. Est-ce vous même ?"

        if service_type and service_uri:
            service_label = service_type

            # Format service URI
            if service_type == "email":
                service_label = "Email"
                service_uri = "mailto:%s" % service_uri
            elif service_type == "xmpp":
                service_label = "XMPP"
                service_uri = "xmpp:%s" % service_uri
            elif service_type == "irc":
                service_label = "IRC"
                service_uri = "irc:%s" % service_uri
            elif service_type == "web":
                service_label = "Web"
            elif service_type == "facebook":
                service_label = "Facebook"

            DiapasonService.objects.get_or_create(diapason=response_data['diapason'], label=service_label, uri=service_uri)

            try:
                response_data['alert_response_data_success'] = True

                # Refresh response data
                response_data = AssemblyHelper.assembly_build_response(request, diapason_name)
            except:
                response_data['alert_response_data_error'] = "Le service n'a pu être ajouté"

    return render(request, 'diapason/diapason_assembly.jade', response_data)


@auth_required
@member_required
def assembly_session_proposal(request, diapason_name, diapason_session, diapason_proposal):
    """
    Diapason > Assembly Session Proposal
    """
    response_data = AssemblyHelper.session_proposal_build_response(request, diapason_name, diapason_session, diapason_proposal)

    if response_data['status'] == 'not_found':
        raise Http404

    # Submit proposal?
    if request.method == 'POST' and response_data['can_participate'] is True:
        choice_title = request.POST.get('choice_title', None)
        vote_choice_id = request.POST.get('vote_choice_id', None)

        if choice_title is not None:
            if choice_title:
                choice_already_exists = False

                # Build fuzzy match regex
                match_term = ".*"

                for term in choice_title.split(" "):
                    match_term = match_term + ("(?=.*" + term + ".*)")

                match_term = match_term + ".*"

                choice_regex = re.compile(match_term.upper())

                # Proceed text mining on already existing proposals
                existing_proposals = DiapasonProposalChoice.objects.filter(proposal=response_data['proposal'])

                for proposal in existing_proposals:
                    if proposal.value and choice_regex.match(proposal.value.upper()):
                        choice_already_exists = True

                # Insert proposal?
                if choice_already_exists is False:
                    DiapasonProposalChoice.objects.get_or_create(proposal=response_data['proposal'], value=choice_title)

                    try:
                        response_data['alert_choice_response_data_success'] = True

                        # Refresh response data
                        response_data = AssemblyHelper.session_proposal_build_response(request, diapason_name, diapason_session, diapason_proposal)
                    except:
                        response_data['alert_choice_response_data_error'] = True
                else:
                    response_data['alert_choice_response_data_conflict'] = True
            else:
                response_data['alert_choice_response_data_error'] = True
        elif vote_choice_id is not None:
            if vote_choice_id:
                choice = DiapasonProposalChoice.objects.get(proposal=response_data['proposal'], id=vote_choice_id)

                # Aggregate the list of delegated users
                delegated_users = [request.user]

                # Expand delegated users
                tags = [tag.strip() for tag in response_data['session'].tags.split(',')]

                AssemblyHelper.expand_delegated(delegated_users, request.user, response_data['diapason'], tags)

                # Vote for each user
                for user in delegated_users:
                    from_delegation = True if user.id != request.user.id else False

                    # Get previous vote on same value
                    prev_vote = DiapasonProposalChoiceVote.objects.filter(proposal=response_data['proposal'], choice=choice, voter=user).first()

                    if prev_vote and not from_delegation:
                        # Nuke this vote? (or do not touch)
                        if prev_vote.voter == request.user or prev_vote.from_delegation:
                            prev_vote.delete()
                    else:
                        # Insert new vote
                        DiapasonProposalChoiceVote.objects.get_or_create(proposal=response_data['proposal'], choice=choice, voter=user, from_delegation=from_delegation)
                        DiapasonSessionMember.objects.get_or_create(diapason_session=response_data['session'], user=user)

                # Refresh response data
                response_data = AssemblyHelper.session_proposal_build_response(request, diapason_name, diapason_session, diapason_proposal)

                response_data['alert_vote_response_data_success'] = True
            else:
                response_data['alert_vote_response_data_error'] = True

    return render(request, 'diapason/diapason_assembly_proposal.jade', response_data)
