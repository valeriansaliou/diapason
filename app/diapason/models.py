# -*- coding: utf-8 -*-

import re

from django.db import models
from django_enumfield import enum

from django.contrib.auth.models import User as AuthUser

from _commons.forms import defaults


class VisibilityLevels(enum.Enum):
    PUBLIC = 0
    PROTECTED = 1
    PRIVATE = 2

    labels = {
        PUBLIC: 'Public',
        PROTECTED: 'Protégé',
        PRIVATE: 'Privé',
    }

class Periodicities(enum.Enum):
    DAILY = 0
    BIWEEKLY = 1
    WEEKLY = 2
    BIMONTHLY = 3
    MONTHLY = 4

    labels = {
        DAILY: 'Chaque jour',
        BIWEEKLY: 'Deux fois par semaine',
        WEEKLY: 'Chaque semaine',
        BIMONTHLY: 'Deux fois par mois',
        MONTHLY: 'Chaque mois',
    }

class Statuses(enum.Enum):
    OPENED = 0
    CLOSED = 1

    labels = {
        OPENED: 'Session ouverte',
        CLOSED: 'Session closed',
    }

class ProposalsPerCycle(enum.Enum):
    ONE = 0
    TWO = 1
    THREE = 2
    FOUR = 3
    FIVE_MORE = 4

    labels = {
        ONE: 'Une seule',
        TWO: 'Deux',
        THREE: 'Trois',
        FOUR: 'Quatre',
        FIVE_MORE: 'Cinq et plus',
    }

class MembershipLevel(enum.Enum):
    MEMBER = 0
    ADMIN = 1

    labels = {
        MEMBER: 'Membre',
        ADMIN: 'Admin',
    }



# Diapason: stores diapasons
class Diapason(models.Model):
    """
    Database [diapason]
    """

    slug = models.CharField(db_index=True, max_length=36, primary_key=True)
    name = models.TextField(default="", blank=False, null=False)
    description = models.TextField(default="", blank=False, null=False)
    visibility_level = enum.EnumField(VisibilityLevels, default=VisibilityLevels.PUBLIC)

    proposals_per_cycle = enum.EnumField(ProposalsPerCycle, default=ProposalsPerCycle.ONE)

    # users / members (many to one)
    # cycles (many to one)

    date_created = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s %s %s' % (self.user.first_name, self.user.last_name, self.user.profile.city)


# DiapasonMember: stores Diapason membership
class DiapasonMember(models.Model):
    """
    Database [diapason.member]
    """
    user = models.ForeignKey(AuthUser)
    diapason = models.ForeignKey(Diapason)

    level = enum.EnumField(MembershipLevel, default=MembershipLevel.MEMBER)

    def __unicode__(self):
        return u'%i->%i' % (self.user, self.diapason)


# DiapasonDelegation: stores Diapason delegation
class DiapasonDelegation(models.Model):
    """
    Database [diapason.delegation]
    """
    user = models.ForeignKey(AuthUser, related_name='user')
    delegated_user = models.ForeignKey(AuthUser, related_name='delegated_user')
    diapason = models.ForeignKey(Diapason)
    subject = models.TextField(default="", blank=False, null=False)

    def __unicode__(self):
        return u'%i' % (self.user)


# DiapasonService: stores Diapason services
class DiapasonService(models.Model):
    """
    Database [diapason.service]
    """
    diapason = models.ForeignKey(Diapason)

    label = models.TextField(default="", blank=False, null=False)
    uri = models.TextField(default="", blank=False, null=False)

    def __unicode__(self):
        return u'%i->%i' % (self.label, self.uri)


# DiapasonSession: stores Diapason session
class DiapasonSession(models.Model):
    """
    Database [diapason.session]
    """
    author = models.ForeignKey(AuthUser)
    diapason = models.ForeignKey(Diapason)

    name = models.TextField(default="", blank=False, null=False)
    topic = models.TextField(default="", blank=False, null=False)
    tags = models.TextField(default="", blank=False, null=False)

    status = enum.EnumField(Statuses, default=Statuses.OPENED)

    date_created = models.DateTimeField(auto_now_add=True)
    date_autoclose = models.DateTimeField(null=True)
    date_closed = models.DateTimeField(null=True)

    def __unicode__(self):
        return u'%i->%i' % (self.author, self.diapason)


# DiapasonSessionMember: stores Diapason session membership
class DiapasonSessionMember(models.Model):
    """
    Database [diapason.member]
    """
    user = models.ForeignKey(AuthUser)
    diapason_session = models.ForeignKey(DiapasonSession)

    def __unicode__(self):
        return u'%i->%i' % (self.user, self.session)


# DiapasonProposal: stores Diapason proposals
class DiapasonProposal(models.Model):
    """
    Database [diapason.proposal]
    """
    author = models.ForeignKey(AuthUser)
    diapason_session = models.ForeignKey(DiapasonSession)

    name = models.TextField(default="", blank=False, null=False)

    def __unicode__(self):
        return u'%i->%i' % (self.author, self.diapason_session)


# DiapasonProposalChoice: stores Diapason proposal choices
class DiapasonProposalChoice(models.Model):
    """
    Database [diapason.proposal.choice]
    """
    proposal = models.ForeignKey(DiapasonProposal)

    value = models.TextField(default="", blank=False, null=False)

    def __unicode__(self):
        return u'%i->%i' % (self.proposal, self.value)


# DiapasonProposalChoiceVote: stores Diapason proposal choice vote
class DiapasonProposalChoiceVote(models.Model):
    """
    Database [diapason.proposal.choice]
    """
    voter = models.ForeignKey(AuthUser)
    proposal = models.ForeignKey(DiapasonProposal)
    choice = models.ForeignKey(DiapasonProposalChoice)
    from_delegation = models.BooleanField(default=False)

    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%i->[%i]->%i' % (self.proposal, self.voter, self.choice)


# DiapasonInvite: stores Diapason invites
class DiapasonInvite(models.Model):
    """
    Database [diapason.invite]
    """
    diapason = models.ForeignKey(Diapason)

    secret = models.TextField(default="", blank=False, null=False)

    def __unicode__(self):
        return u'invite->%i' % (self.diapason)
