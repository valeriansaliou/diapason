from django.conf.urls import patterns

urlpatterns = patterns('diapason.views',
    (r'^new/$', 'new'),
    (r'^new/user/search/(?P<query>.+)/$', 'new_user_search'),
    (r'^invite/$', 'invite'),
    (r'^assembly/(?P<diapason_name>[\w\.-]+)/$', 'assembly'),
    (r'^assembly/(?P<diapason_name>[\w\.-]+)/session/(?P<diapason_session>[\w-]+)/proposal/(?P<diapason_proposal>[\w-]+)/$', 'assembly_session_proposal'),
)
