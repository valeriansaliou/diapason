# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Member'
        db.delete_table(u'diapason_member')

        # Deleting model 'Proposal'
        db.delete_table(u'diapason_proposal')

        # Adding model 'DiapasonSession'
        db.create_table(u'diapason_diapasonsession', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('diapason', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['diapason.Diapason'])),
            ('name', self.gf('django.db.models.fields.TextField')(default='')),
        ))
        db.send_create_signal(u'diapason', ['DiapasonSession'])

        # Adding model 'DiapasonProposal'
        db.create_table(u'diapason_diapasonproposal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('diapason_session', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['diapason.DiapasonSession'])),
            ('name', self.gf('django.db.models.fields.TextField')(default='')),
        ))
        db.send_create_signal(u'diapason', ['DiapasonProposal'])

        # Adding model 'DiapasonMember'
        db.create_table(u'diapason_diapasonmember', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('diapason', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['diapason.Diapason'])),
            ('level', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'diapason', ['DiapasonMember'])


    def backwards(self, orm):
        # Adding model 'Member'
        db.create_table(u'diapason_member', (
            ('diapason', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['diapason.Diapason'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('level', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'diapason', ['Member'])

        # Adding model 'Proposal'
        db.create_table(u'diapason_proposal', (
            ('diapason', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['diapason.Diapason'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')(default='')),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'diapason', ['Proposal'])

        # Deleting model 'DiapasonSession'
        db.delete_table(u'diapason_diapasonsession')

        # Deleting model 'DiapasonProposal'
        db.delete_table(u'diapason_diapasonproposal')

        # Deleting model 'DiapasonMember'
        db.delete_table(u'diapason_diapasonmember')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'diapason.diapason': {
            'Meta': {'object_name': 'Diapason'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'periodicity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'priorities_per_cycle': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'proposals_per_cycle': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '36', 'primary_key': 'True', 'db_index': 'True'}),
            'visibility_level': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'diapason.diapasonmember': {
            'Meta': {'object_name': 'DiapasonMember'},
            'diapason': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['diapason.Diapason']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'diapason.diapasonproposal': {
            'Meta': {'object_name': 'DiapasonProposal'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'diapason_session': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['diapason.DiapasonSession']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'diapason.diapasonsession': {
            'Meta': {'object_name': 'DiapasonSession'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'diapason': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['diapason.Diapason']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        }
    }

    complete_apps = ['diapason']