import math

from random import shuffle
from datetime import datetime

from django.contrib.auth.models import User as AuthUser
from pyvotecore.schulze_method import SchulzeMethod

from diapason.models import Diapason, DiapasonInvite, DiapasonDelegation, DiapasonSession, DiapasonMember, DiapasonProposal, DiapasonService, DiapasonSessionMember, DiapasonProposalChoice, DiapasonProposalChoiceVote


class InviteHelper(object):
    """
    An invite on Diapason operations
    """

    @staticmethod
    def check_and_accept_invite(request):
        key = request.GET.get('key', '').strip()

        invite = DiapasonInvite.objects.filter(secret=key)

        if invite and invite[0]:
            # Add membership to Diapason
            diapason = invite[0].diapason

            # Check if member isnt already in.
            if DiapasonMember.objects.filter(diapason=diapason, user=request.user).count() == 0:
                member = DiapasonMember(
                    diapason=diapason,
                    user=request.user,
                    level=0
                )
                member.save()

            invite[0].delete()

            return diapason

        return None


class AssemblyHelper(object):
    """
    An helper on Diapason Assembly operations
    """

    @classmethod
    def expand_delegated(self, expanded, user, diapason, tags):
        # Expand tree
        delegated_users = [item.user for item in DiapasonDelegation.objects.filter(diapason=diapason, delegated_user=user, subject__in=tags)]

        for delegated_user in delegated_users:
            if not delegated_user in expanded:
                expanded.append(delegated_user)

                self.expand_delegated(expanded, user, diapason, tags)


    @staticmethod
    def assembly_build_response(request, diapason_name):
        """
        Builds the assembly main response
        """
        # Response data object
        response_data = {
            'status': 'none',
            'can_participate': True,
        }

        try:
            diapason = Diapason.objects.get(slug=diapason_name)
        except Diapason.DoesNotExist:
            response_data['status'] = 'not_found'
            return response_data

        try:
            member = DiapasonMember.objects.get(diapason=diapason, user=request.user)
            response_data['is_member'] = True
        except DiapasonMember.DoesNotExist:
            member = None
            response_data['is_member'] = False

        today = datetime.today()

        all_sessions = DiapasonSession.objects.filter(diapason=diapason.slug).order_by('-date_autoclose')
        past_sessions = DiapasonSession.objects.filter(diapason=diapason.slug, date_autoclose__lte=today).order_by('-date_autoclose')
        ongoing_sessions = DiapasonSession.objects.filter(diapason=diapason.slug, date_autoclose__gt=today).order_by('date_autoclose')
        diapason_services = DiapasonService.objects.filter(diapason=diapason.slug)

        response_data['delegated'] = DiapasonDelegation.objects.filter(diapason=diapason.slug, user=request.user)
        response_data['delegated_count'] = len(response_data['delegated'])

        # Resolve participation level?
        if diapason.visibility_level >= 1:
            # Not member?
            if response_data['is_member'] == False:
                response_data['can_participate'] = False

        # Resolve first members of Diapason (if any)
        diapason_first_members = DiapasonMember.objects.filter(diapason=diapason)[:3]
        diapason_remaining_members_count = (DiapasonMember.objects.filter(diapason=diapason).count() - len(diapason_first_members))

        # Resolve first session (active session)
        active_session = None

        if len(ongoing_sessions) > 0:
            active_session = ongoing_sessions[0]

        # Resolve first members of active session (if any)
        active_session_first_members = []
        active_session_remaining_members_count = 0

        if active_session:
            # Set maximum to 5 members
            active_session_first_members = DiapasonSessionMember.objects.filter(diapason_session=active_session)[:5]
            active_session_remaining_members_count = (DiapasonSessionMember.objects.filter(diapason_session=active_session).count() - len(active_session_first_members))

        # Resolve proposals of active session
        active_session_proposals = []

        if active_session:
            active_session_proposals = DiapasonProposal.objects.filter(diapason_session=active_session)[::1]

            # Prioritize
            shuffle(active_session_proposals)

            # Check if session is still active
            active_session.is_still_active = True

        # Resolve proposals for past sessions
        diapason_past_sessions = past_sessions

        for past_session in diapason_past_sessions:
            past_session.proposals = DiapasonProposal.objects.filter(diapason_session=past_session)

            # Check if session is still active
            past_session.is_still_active = False

        has_active_session_first_members = (True if len(active_session_first_members) > 0 else False)

        response_data.update({
            'status': 'success',
            'member': member,
            'diapason': diapason,
            'diapason_first_members': diapason_first_members,
            'diapason_remaining_members_count': diapason_remaining_members_count,
            'active_session': active_session,
            'active_session_first_members': active_session_first_members,
            'has_active_session_first_members': has_active_session_first_members,
            'active_session_remaining_members_count': active_session_remaining_members_count,
            'active_session_proposals': active_session_proposals,
            'all_sessions': all_sessions,
            'past_sessions': diapason_past_sessions,
            'services': diapason_services,
        })

        return response_data


    @staticmethod
    def session_proposal_build_response(request, diapason_name, diapason_session, diapason_proposal):
        """
        Builds the proposal main response
        """
        # Response data object
        response_data = {
            'status': 'none',
            'can_participate': True,
            'winner': None,
        }

        try:
            diapason = Diapason.objects.get(slug=diapason_name)
        except Diapason.DoesNotExist:
            response_data['status'] = 'not_found'
            return response_data

        try:
            member = DiapasonMember.objects.get(diapason=diapason, user=request.user)
            response_data['is_member'] = True
        except DiapasonMember.DoesNotExist:
            response_data['is_member'] = False

        try:
            diapason_session_instance = DiapasonSession.objects.get(diapason=diapason_name, id=diapason_session)
        except DiapasonSession.DoesNotExist:
            response_data['status'] = 'not_found'
            return response_data

        # Acquire proposal for session
        try:
            diapason_proposal_instance = DiapasonProposal.objects.get(diapason_session=diapason_session_instance, id=diapason_proposal)
        except DiapasonProposal.DoesNotExist:
            response_data['status'] = 'not_found'
            return response_data

        # Resolve participation level?
        if diapason.visibility_level >= 1:
            # Not member?
            if response_data['is_member'] == False:
                response_data['can_participate'] = False

        # Check if session is still active
        diapason_session_is_still_active = True

        if datetime.now() >= diapason_session_instance.date_autoclose:
            diapason_session_is_still_active = False

        # Resolve first members of Diapason (if any)
        diapason_first_members = DiapasonMember.objects.filter(diapason=diapason)[:3]
        diapason_remaining_members_count = (DiapasonMember.objects.filter(diapason=diapason).count() - len(diapason_first_members))

        # Resolve proposal choices
        diapason_proposal_choices = DiapasonProposalChoice.objects.filter(proposal=diapason_proposal_instance)
        linear_diapason_proposal_choices = [choice.value for choice in diapason_proposal_choices]

        # Count total voters for proposal
        proposal_voters = DiapasonProposalChoiceVote.objects.filter(proposal=diapason_proposal_instance).values("voter").distinct()
        total_proposal_voters = proposal_voters.count()

        # Resolve each choice votes
        for diapason_proposal_choice in diapason_proposal_choices:
            # Check if user voted for this choice
            current_proposal_choice_local_user = DiapasonProposalChoiceVote.objects.filter(choice=diapason_proposal_choice, voter=request.user).count()

            diapason_proposal_choice.has_voted = "true" if current_proposal_choice_local_user > 0 else "false"
            diapason_proposal_choice.vote_order = -1

            # Calculate percentage of voters for this choice
            current_proposal_choice_voters = DiapasonProposalChoiceVote.objects.filter(choice=diapason_proposal_choice)

            diapason_proposal_choice.voters = []

            for current_proposal_choice_voters_one in current_proposal_choice_voters:
                diapason_proposal_choice.voters.append(current_proposal_choice_voters_one.voter)

            diapason_proposal_choice.percent_voted = int(0 if (total_proposal_voters is 0) else math.floor(len(current_proposal_choice_voters) / total_proposal_voters)) * 100

            diapason_proposal_choice.voters_len = len(diapason_proposal_choice.voters)
            diapason_proposal_choice.voters_len_idx = (diapason_proposal_choice.voters_len - 1)

        # Process user vote order
        voter_ordered_choices_votes = {}

        if total_proposal_voters > 0:
            # For each voter
            for proposal_voter in proposal_voters:
                # Order voter choices
                voter_choices = [vote.choice.value for vote in DiapasonProposalChoiceVote.objects.filter(proposal=diapason_proposal_instance, voter=proposal_voter['voter']).order_by('date_created')]

                if len(voter_choices) > 0:
                    voter_ordered_choices_votes[proposal_voter['voter']] = voter_choices

        # Assign user vote order
        if request.user.id in voter_ordered_choices_votes:
            for diapason_proposal_choice in diapason_proposal_choices:
                for idx, voted_choice in enumerate(voter_ordered_choices_votes[request.user.id]):
                    if diapason_proposal_choice.value == voted_choice:
                        diapason_proposal_choice.vote_order = (idx + 1)

        # Process winner choice
        if len(voter_ordered_choices_votes) > 0:
            # Process vote input
            # eg: [{ "count":3, "ballot":[["A"], ["C"], ["D"], ["B"]] }]

            # Linearize
            linear_voter_ordered_choices_votes = []

            for voter_ordered_choices_vote in voter_ordered_choices_votes.values():
                linear_voter_ordered_choices_votes.append(voter_ordered_choices_vote)

            # Reduce linearized list to vote input
            vote_input = []

            for linear_voter_ordered_choices_vote in linear_voter_ordered_choices_votes:
                was_found = False

                for vote_input_one in vote_input:
                    if vote_input_one['ballot'] == linear_voter_ordered_choices_vote:
                        vote_input_one['count'] = (vote_input_one['count'] + 1)
                        was_found = True

                        break

                if was_found is False:
                    vote_input.append({
                        "count" : 1,
                        "ballot" : linear_voter_ordered_choices_vote,
                    })

            # Convert list to sub arrays (ballot format)
            for vote_input_one in vote_input:
                vote_input_one['ballot'] = [[value] for value in vote_input_one['ballot']]

            if len(vote_input) > 0:
                vote_result =  SchulzeMethod(vote_input, ballot_notation = "grouping").as_dict()

                response_data['winner'] = ', '.join(vote_result['tied_winners']) if ('tied_winners' in vote_result) else vote_result['winner']

        response_data.update({
            'status': 'success',
            'diapason': diapason,
            'diapason_first_members': diapason_first_members,
            'diapason_remaining_members_count': diapason_remaining_members_count,
            'diapason_proposal_choices': diapason_proposal_choices,
            'session': diapason_session_instance,
            'active_session': diapason_session_instance,
            'is_still_active' : diapason_session_is_still_active,
            'proposal': diapason_proposal_instance,
            'total_proposal_voters': total_proposal_voters,
        })

        return response_data
