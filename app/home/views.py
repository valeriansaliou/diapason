from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from _commons.decorators.security import auth_required

from django.shortcuts import render

from diapason.models import Diapason


@auth_required
def root(request):
    """
    Home > Root
    """
    latest = Diapason.objects.all().order_by('-date_created')[:10:1]

    return render(request, 'home/home_root.jade', {
      'latest' : latest,
    })
