from django.conf.urls import url, patterns, include

urlpatterns = patterns('',
    # Most visited pages
    (r'^$', include('home.urls')),
)

urlpatterns += patterns('account.views',
    url(r'^login/$', 'login_root'),
    (r'^logout/$', 'logout_root'),

    (r'^register/$', 'register_root'),
    url(r'^register/go/$', 'register_go'),
    url(r'^register/profile/$', 'register_profile'),
    url(r'^register/about/$', 'register_about'),
    url(r'^register/done/$', 'register_done'),

    url(r'^settings/$', 'settings_root'),
    url(r'^settings/credentials/$', 'settings_credentials'),
    url(r'^settings/ajax/avatar/$', 'settings_ajax_avatar'),
)

urlpatterns += patterns('',
    url(r'^login/(?P<backend>[^/]+)/$', 'account.views.login_social'),
    (r'', include('social.apps.django_app.urls', namespace='social')),
)
