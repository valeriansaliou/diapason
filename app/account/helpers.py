import os
from datetime import datetime, timedelta
from django.contrib.auth.models import User as AuthUser

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import load_backend, login
from django.contrib.auth.tokens import default_token_generator as token_generator

from .models import *



class LoginHelper(object):
    """
    An helper on login operations
    """

    @staticmethod
    def login(request, user):
        """
        Login a given user
        """
        # Login user w/o required credentials (but rather the model object)
        if not hasattr(user, 'backend'):
            for backend in settings.AUTHENTICATION_BACKENDS:
                if user == load_backend(backend).get_user(user.pk):
                    user.backend = backend
                    break
        if hasattr(user, 'backend'):
            return login(request, user)



class RegisterHelper(object):
    """
    An helper on register operations
    """

    steps = [
        'go',
        'profile',
        'about',
        'done'
    ]


    @classmethod
    def step_name(_class, step_id):
        """
        Return the register step name associated to a step ID
        """
        try:
            assert type(step_id) is int
            return _class.steps[step_id]
        except IndexError:
            return _class.steps[1]


    @classmethod
    def step(_class, request):
        """
        Return the current register step
        """
        step_current = 1

        try:
            step_current = Register.objects.get(user=request.user).step_current
        except Register.DoesNotExist:
            pass
        finally:
            return _class.step_name(step_current)


    @staticmethod
    def update_resume(request):
        """
        Increment the 'registration resumed' counter
        """
        resumed_count = 0

        try:
            register = Register.objects.get(user=request.user)
            resumed_count = register.resumed_count
            register.resumed_count += 1
            register.save()
        except Register.DoesNotExist:
            pass
        finally:
            return resumed_count


    @staticmethod
    def is_resumed(request):
        """
        Return whether registration was resumed or not
        """
        try:
            return True if (Register.objects.get(user=request.user).resumed_count > 0) else False
        except Register.DoesNotExist:
            return False
