import social, facebook

from django.conf import settings

from avatar.models import Binding as AvatarBinding
from avatar.helpers import AvatarHelper

from .models import *
from .factories import *


def get_username(backend, uid, details, strategy, user=None, *args, **kwargs):
    """
    Gets the user username from social account data
    """
    if not user:
        first_name = details.get('first_name', None)
        last_name = details.get('last_name', None)

        if (first_name and last_name) or (uid and backend):
            final_username = UserFactory(user).generate_username(
                first_name=first_name,
                last_name=last_name,
                uid=uid,
                backend=backend,
            )
        else:
            raise Exception('Could Not Generate Username (First Name/Last Name/UID/Backend Missing)')
    else:
        final_username = strategy.storage.user.get_username(user)

    return {
        'username': final_username
    }


def update_user_avatar(backend, response, user, is_new=False, *args, **kwargs):
    """
    Updates the user avatar from social account
    """
    if is_new and user:
        AvatarHelper.update(user, source=backend.name)


def fill_profile_user(backend, response, details, user, is_new=False, *args, **kwargs):
    """
    Auto-confirm user that comes from a social API
    """
    if is_new and user and user.is_authenticated():
        social_user = user.social_auth.filter(provider=backend.name).first()

        if not social_user:
            raise Exception("Social User Binding Not Found")

        # Get models
        profile = Profile.objects.get_or_create(user=user)[0]
        register = Register.objects.get_or_create(user=user)[0]

        if backend.name == 'facebook':
            # Get data from Facebook Graph
            graph = facebook.GraphAPI(social_user.extra_data['access_token'])
            graph_data = graph.fql('SELECT {rows} FROM {table} WHERE uid=me()'.format(
                rows=', '.join([
                    'first_name',
                    'last_name',
                    'user_about_me',
                    'verified'
                ]),
                table='user',
            ))

            if len(graph_data):
                graph_data = graph_data[0]
            else:
                raise Exception("Got No Response Data From Facebook Graph")

            # Get user first name & last name
            user.first_name = graph_data['first_name'].strip().title()
            user.last_name = graph_data['last_name'].strip().title()

            # Get email verified state (we trust Facebook)
            profile.email_verified = profile.email_verified or (graph_data['verified'] and True)
        else:
            raise ValueError()

        # Check for registration form status
        if not user.email:
            register.step_current = 0
        elif not user.first_name or not user.last_name:
            register.step_current = 1
        elif not profile.about:
            register.step_current = 2
        else:
            register.step_current = 3

        # Store data
        profile.save()
        register.save()
        user.save()


def remove_avatar_binding(backend, user, *args, **kwargs):
    """
    Removes the social account avatar binding (if set to disconnected backend)
    """
    if backend and user:
        try:
            if user.binding.source == backend.name:
                # Re-initialize avatar to initial state
                AvatarHelper.initialize(user)
        except AvatarBinding.DoesNotExist:
            pass
