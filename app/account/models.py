import re

from django.db import models
from django.contrib.auth.models import User as AuthUser

from _commons.forms import defaults


# Profile: stores user profile
class Profile(models.Model):
    """
    Database [account.profile]
    """
    # System fields
    user = models.OneToOneField(AuthUser, primary_key=True)

    # About field
    about = models.TextField(default="")

    # Verification status
    register_complete = models.BooleanField(default=False)

    date_update = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s %s %s' % (self.user.first_name, self.user.last_name, self.user.profile.city)


# Register: stores registration information (for metrics + abuse control)
class Register(models.Model):
    """
    Database [account.register]
    """
    # System fields
    user = models.OneToOneField(AuthUser, primary_key=True)

    # Registration fields
    complete = models.BooleanField(default=False)
    step_current = models.PositiveSmallIntegerField(default=1)
    resumed_count = models.PositiveSmallIntegerField(default=0)
    date_start = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)
    date_complete = models.DateTimeField(blank=True, null=True)
    ip_start = models.GenericIPAddressField(blank=True, null=True)
    ip_update = models.GenericIPAddressField(blank=True, null=True)
    ip_complete = models.GenericIPAddressField(blank=True, null=True)

    def __unicode__(self):
        return u'%i' % (self.complete)


# Settings: stores user settings
class Settings(models.Model):
    """
    Database [account.settings]
    """
    # System fields
    user = models.OneToOneField(AuthUser, primary_key=True)

    date_update = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s %s' % (self.user.first_name, self.user.last_name)
