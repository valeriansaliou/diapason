# -*- coding: utf-8 -*-

from django import forms
from _commons.forms import defaults
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from _commons.validators import _is_valid
from _commons.forms.errors import field_error

from .models import *


# -- Helper: check if passwords match
def _clean_password_confirm(self, first_input='password', second_input='password_confirm'):
    password = self.cleaned_data.get(first_input)
    password_confirm = self.cleaned_data.get(second_input)

    if not password_confirm:
        raise forms.ValidationError('Please confirm your password (enter it again).', 'required')
    if password_confirm != password:
        raise forms.ValidationError('Passwords do not match', 'invalid')

    return password_confirm


# -- Helper: check if email is acceptable
def _clean_email(self, field='email'):
    """
    Clean an email in a form field
    """
    value = self.cleaned_data.get(field)
    user = User.objects.filter(email=value)

    if user.count()\
       and (not hasattr(self, 'uid') or (not self.uid in [u.id for u in user])):
        error_message = "L'email est déjà utilisé."

        if field and self.fields:
            field_error(self, field, error_message)
        else:
            raise ValidationError(error_message, 'taken')

    return value


# -- Helper: clean all values
def _clean_entitle(self, field):
    """
    Clean a form field value
    """
    value = self.cleaned_data.get(field)

    if value:
        value = value.title().strip()

    return value


# -- Helper: validate after stripping value
def _validate_strip(value):
    if not value or not value.strip():
        raise ValidationError(None, 'required')


# -- Login: Root
class LoginRootForm(forms.Form):
    """
    Account > Login Root Form
    """

    username = forms.CharField(
        required=True,
        widget=forms.TextInput(
        attrs={
            'class': 'email',
            'placeholder': 'Entrez votre email',
            'pattern': defaults.PATTERN_USERNAME

        })
    )

    password = forms.CharField(
        required=True,
        min_length=defaults.PWD_LENGTH_MIN,
        max_length=defaults.PWD_LENGTH_MAX,
        widget=forms.PasswordInput(
        attrs={
            'class': 'password',
            'placeholder': 'Entrez votre mot de passe',
            'pattern': defaults.PATTERN_PWD
        })
    )

    remember = forms.BooleanField(
        initial=False,
        required=False,
        widget=forms.CheckboxInput(attrs={ 'id': 'rememberMe', 'class': 'checkbox-slide'})
    )

    def is_valid(self):
        return _is_valid(self, forms.Form)


# -- Register: Step 1 (go)
class RegisterGoForm(forms.Form):
    """
    Account > Register Go Form
    """
    def __init__(self, *args, **kwargs):
        self.has_password = kwargs.pop('has_password') if 'has_password' in kwargs else True
        super(forms.Form, self).__init__(*args, **kwargs)

        self.fields['email'] = forms.EmailField(
            required=True,

            widget=forms.TextInput(
                attrs={
                    'type': 'email',
                    'class': 'email',
                    'placeholder': 'Votre adresse email',
                    'pattern': defaults.PATTERN_EMAIL
                }
            ),

            error_messages={
                'required': 'Veuillez entrer votre adresse email.',
                'invalid': 'Entrez une adresse email valide.'
            }
        )

        if self.has_password:
            self.fields['password'] = forms.CharField(
                required=True,
                min_length=defaults.PWD_LENGTH_MIN,
                max_length=defaults.PWD_LENGTH_MAX,

                widget=forms.PasswordInput(
                    attrs={
                        'class': 'password',
                        'placeholder': 'Votre mot de passe',
                        'pattern': defaults.PATTERN_PWD,
                        'data-equal': 'password_confirm',
                        'data-equal-redirect': '1'
                    }
                ),

                error_messages={
                    'required': 'Veuillez entrer votre mot de passe.',
                    'invalid': 'Mot de passe trop court (de ' + str(defaults.PWD_LENGTH_MIN) + ' à ' + str(defaults.PWD_LENGTH_MAX) + ' caractères).'
                }
            )

            self.fields['password_confirm'] = forms.CharField(
                required=True,
                min_length=defaults.PWD_LENGTH_MIN,
                max_length=defaults.PWD_LENGTH_MAX,

                widget=forms.PasswordInput(
                    attrs={
                        'class': 'password-confirm',
                        'placeholder': 'Confirmez votre mot de passe',
                        'pattern': defaults.PATTERN_PWD,
                        'data-equal': 'password'
                    }
                ),

                error_messages={
                    'required': 'Veuillez confirmer votre mot de passe (entrez-le à nouveau).',
                    'invalid': 'Les mots de passe ne correspondent pas'
                }
            )

        self.fields['terms_agree'] = forms.BooleanField(
            initial=True,

            widget=forms.CheckboxInput(
                attrs={
                    'id': 'termsCb',
                    'class': 'checkbox-classic'
                }
            ),

            error_messages={
                'required': 'Veuillez agréer à nos termes.'
            }
        )

    def clean_password_confirm(self):
        if self.has_password:
            return _clean_password_confirm(self)

    def clean_email(self):
        return _clean_email(self)

    def is_valid(self):
        return _is_valid(self, forms.Form)


# -- Register: Step 2 (profile)
class RegisterProfileForm(forms.Form):
    """
    Account > Register Profile Form
    """
    first_name = forms.CharField(
        required=True,
        validators=[_validate_strip],
        max_length=defaults.DEFAULT_MAX,

        widget=forms.TextInput(
            attrs={
                'class': 'first-name',
                'placeholder': 'Votre prénom'
            }
        ),

        error_messages={
            'required': 'Entrez votre prénom.',
            'invalid': 'Votre prénom semble invalide.'
        }
    )

    last_name = forms.CharField(
        required=True,
        validators=[_validate_strip],
        max_length=defaults.DEFAULT_MAX,

        widget=forms.TextInput(
            attrs={
                'class': 'last-name',
                'placeholder': 'Votre nom de famille'
            }
        ),

        error_messages={
            'required': 'Entrez votre nom de famille.',
            'invalid': 'Votre nom de famille semble invalide.'
        }
    )

    def clean_first_name(self):
        return _clean_entitle(self, 'first_name')

    def clean_last_name(self):
        return _clean_entitle(self, 'last_name')

    def is_valid(self):
        return _is_valid(self, forms.Form)


# -- Register: Step 3 (about)
class RegisterAboutForm(forms.Form):
    """
    Account > Register About Form
    """
    about = forms.CharField(
        required=True,

        widget=forms.Textarea(
            attrs={
                'cols': 42,
                'rows': 20,
                'class': 'about',
                'placeholder': 'Décrivez-vous en quelques mots'
            }
        ),

        error_messages={
            'required': 'Entrez votre description en quelques mots.',
            'invalid': 'Votre description semble invalide.'
        }
    )

    def is_valid(self):
        return _is_valid(self, forms.Form)


# -- Settings: Root
class SettingsRootUserForm(forms.ModelForm):
    """
    Account > Settings Root Form (User)
    """
    first_name = forms.CharField(
        required=True,
        validators=[_validate_strip],
        max_length=defaults.DEFAULT_MAX,

        widget=forms.TextInput(
            attrs={'class': 'input-main'}
        ),

        error_messages={
            'required': "Entrez votre prénom.",
            'invalid': "Prénom invalide.",
        },
    )

    last_name = forms.CharField(
        required=True,
        validators=[_validate_strip],
        max_length=defaults.DEFAULT_MAX,

        widget=forms.TextInput(
            attrs={'class': 'input-main'}
        ),

        error_messages={
            'required': "Entrez votre nom de famille.",
            'invalid': "Nom de famille invalide.",
        },
    )

    email = forms.EmailField(
        required=True,

        widget=forms.EmailInput(
            attrs={'class': 'input-main'}
        ),

        error_messages={
            'required': 'Entrez votre addresse email.',
            'invalid': 'Invalide addresse email.',
        },
    )

    class Meta:
        model = User

        fields = [
            'first_name',
            'last_name',
            'email',
        ]

        labels = {
            'first_name': "Prénom",
            'last_name': "Nom de famille",
            'email': "Email",
        }

        help_texts = {
            'first_name': "Quel est votre nom ?",
            'last_name': "Votre nom de famille ?",
            'email': "Votre email ?",
        }

    def __init__(self, *args, **kwargs):
        self.uid = kwargs.pop('uid') if 'uid' in kwargs else None
        super(SettingsRootUserForm, self).__init__(*args, **kwargs)

    def clean_first_name(self):
        return _clean_entitle(self, 'first_name')

    def clean_last_name(self):
        return _clean_entitle(self, 'last_name')

    def clean_email(self):
        return _clean_email(self)

    def is_valid(self):
        return _is_valid(self, forms.ModelForm)


class SettingsRootProfileForm(forms.ModelForm):
    """
    Account > Settings Root Form (Profile)
    """
    class Meta:
        model = Profile

        fields = [
            'about',
        ]

        labels = {
            'about': "Biographie",
        }

        widgets = {
            'about': forms.Textarea(
                attrs={
                    'cols': 42,
                    'rows': 20,
                    'class': 'textarea-main',
                    'placeholder': 'Décrivez-vous en quelques mots'
                }
            )
        }

    def is_valid(self):
        return _is_valid(self, forms.ModelForm)


# -- Settings: Credentials
class SettingsCredentialsForm(forms.Form):
    """
    Account > Settings Credentials Form
    """
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(forms.Form, self).__init__(*args, **kwargs)

        if self.user.has_usable_password():
            self.fields['current_password'] = forms.CharField(
                required=True,
                validators=[self.validate_current_password],

                widget=forms.PasswordInput(
                    attrs={
                        'class': 'current-password input-main',
                        'placeholder': 'Votre mot de passe courant',
                        'pattern': defaults.PATTERN_PWD,
                    }
                ),

                error_messages={
                    'required': 'Entrez votre mot de passe courant.',
                    'invalid': 'Mot de passe courant invalide.'
                }
            )

        self.fields['new_password'] = forms.CharField(
            required=True,
            min_length=defaults.PWD_LENGTH_MIN,
            max_length=defaults.PWD_LENGTH_MAX,
            help_text="Minimum %s characters." % defaults.PWD_LENGTH_MIN,

            widget=forms.PasswordInput(
                attrs={
                    'class': 'new-password input-main',
                    'placeholder': 'Nouveau Mot de Passe',
                    'pattern': defaults.PATTERN_PWD,
                    'data-equal': 'verify_password',
                    'data-equal-redirect': '1'
                }
            ),

            error_messages={
                'required': 'Entrez votre nouveau mot de passe.',
                'invalid': 'Mot de passe trop court (' + str(defaults.PWD_LENGTH_MIN) + ' to ' + str(defaults.PWD_LENGTH_MAX) + ' caractères).'
            }
        )

        self.fields['verify_password'] = forms.CharField(
            required=True,
            min_length=defaults.PWD_LENGTH_MIN,
            max_length=defaults.PWD_LENGTH_MAX,

            widget=forms.PasswordInput(
                attrs={
                    'class': 'verify-password input-main',
                    'placeholder': 'Confirmez votre mot de passe',
                    'pattern': defaults.PATTERN_PWD,
                    'data-equal': 'new_password'
                }
            ),

            error_messages={
                'required': 'Veuillez entrer à nouveau votre mot de passe.',
                'invalid': 'Les mots de passe ne correspondent pas.'
            }
        )

    def validate_current_password(self, password):
        if not self.user.check_password(password):
            raise ValidationError(None, 'invalid')

    def clean_verify_password(self):
        return _clean_password_confirm(self, 'new_password', 'verify_password')

    def is_valid(self):
        return _is_valid(self, forms.Form)
