from django.conf.urls import patterns, include
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse


# Main URLs
urlpatterns = patterns('',
    # Most visited pages
    (r'^$', include('home.urls')),
    (r'^account/', include('account.urls')),
    (r'^avatar/', include('avatar.urls')),
    (r'^user/', include('user.urls')),
    (r'^diapason/', include('diapason.urls')),
)

# Error URLs
handler400 = '_commons.views.bad_request_400'
handler403 = '_commons.views.forbidden_403'
handler404 = '_commons.views.not_found_404'
handler500 = '_commons.views.server_error_500'
