#!/bin/sh
# -*- coding: utf-8 -*-
''''exec python2 -- "$0" ${1+"$@"} # '''

"""
Diapason setup script
Automatically pull app dependencies and install/update them
"""

from setuptools import setup
import os

os.chdir(os.path.join(os.path.dirname(__file__), '../tmp/eggs/'))

# Fixes an issue with Xcode compiler on MacOS
os.environ['ARCHFLAGS'] = '-Wno-error=unused-command-line-argument-hard-error-in-future'

setup(
    name='diapason',
    version='1.0.0',
    description='Diapason app',
    url='https://gitlab.com/valeriansaliou/diapason',

    author=u'Valerian Saliou',
    author_email='valerian@valeriansaliou.name',

    install_requires=[
        'six==1.8.0',
        'psutil==2.1.3',
        'requests==2.5.0',
        'Pillow==2.6.1',
        'South==1.0.1',
        'pyjade==3.0.0',
        'requests-oauthlib==0.4.2',
        'python-social-auth==0.1.26',
        'facebook-sdk==0.4.0',
        'django==1.6.10',
        'django-enumfield==1.2.1',
        'django-debug-toolbar==1.2.2',
        'python-vote-core===20120423.0'
    ],
)
