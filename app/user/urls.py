from django.conf.urls import patterns

urlpatterns = patterns('user.views',
    (r'^(?P<username>[\w\.-]+)/$', 'main'),
)
