from django.contrib.auth.models import User as AuthUser

from diapason.models import Diapason, DiapasonMember


class MainUserHelper(object):
    """
    An helper on user main operations
    """

    @staticmethod
    def build_response(request, username):
        """
        Builds the user main response
        """
        # Response data object
        response_data = {
            'status': 'none',
        }

        try:
            user = AuthUser.objects.get(username=username)
        except AuthUser.DoesNotExist:
            response_data['status'] = 'not_found'
            return response_data, None

        diapasons = [member.diapason for member in DiapasonMember.objects.filter(user=user)]

        response_data.update({
            'status': 'success',
            'aut_user': user,
            'aut_profile': user.profile,
            'diapasons': diapasons,
        })

        return response_data, user
