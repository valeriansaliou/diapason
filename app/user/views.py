import json

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User as AuthUser

from .helpers import *


def main(request, username):
    """
    User > Main
    """
    response_data, user = MainUserHelper.build_response(request, username)

    if response_data['status'] == 'not_found':
        raise Http404

    return render(request, 'user/user_main.jade', response_data)
