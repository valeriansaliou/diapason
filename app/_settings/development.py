# -*- coding: utf-8 -*-

# Development environment settings

import os, sys
from .common import BASE_DIR, LOGGING


DEBUG = True
TEMPLATE_DEBUG = DEBUG

USE_X_FORWARDED_HOST = True

ALLOWED_HOSTS = ['diapason.org.dev']
ENVIRONMENT = 'development'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.abspath(os.path.join(BASE_DIR, '../../tmp/databases', 'diapason_development.db'))
    }
}

REDIS = {
    'host': 'localhost',
    'port': 6379,
    'db': 4,
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'diapason_development',
        'TIMEOUT': 600,
    }
}

AVATAR_HOST = 'diapason.org.dev'

SITE_URL = 'http://diapason.org.dev/'
AVATAR_URL = 'http://%s/avatar/' % AVATAR_HOST
STATIC_URL = 'http://diapason.org.dev/static/'

SECRET_KEY = '7C6yZ^Q{FL%bs2}8U6*DE8J&d&cc4ZFFAca^^[E99KMUdfk6EK'

STATICFILES_DIRS = ()

# EMAIL_HOST = ''
# EMAIL_HOST_PASSWORD = ''
# EMAIL_HOST_USER = ''
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True

SOCIAL_AUTH_FACEBOOK_KEY = 'OBFUSCATED'
SOCIAL_AUTH_FACEBOOK_SECRET = 'OBFUSCATED'

LOGGING['loggers'] = {
    'django.request': {
        'handlers': ['all_console', 'django_file'],
        'level': 'DEBUG',
        'propagate': True,
    },
    'django.security.DisallowedHost': {
        'handlers': ['null'],
        'propagate': False,
    },
    'console':{
        'level': 'DEBUG',
        'class': 'logging.StreamHandler',
        'stream': sys.stdout
    },
}

DEPLOY = {
    'django': {
        'clean': 'find ./app/ -name "*.pyc" -exec rm -rf {} \;',
        'env': 'if [ ! -f "./env/bin/python" ]; then virtualenv -p /usr/bin/python env --no-site-packages; fi',
        'install': ['./tools/setup.py install', ['retry_error']],
        'syncdb': './tools/manage.py syncdb --noinput',
        'migrate': './tools/manage.py migrate --noinput',
        'collectstatic': './tools/manage.py collectstatic --noinput',
    },

    'static': {
        'install': 'cd ./static/; npm install',
        'clean': None,
        'build': 'cd ./static/; npm run-script build-development',
    },

    'run': {
        'start': './tools/run.py',
        'wait': True,
        'stop': './tools/run.py kill',
    }
}

RUN = [
    ['django', [
        ['server', (
                    './tools/manage.py'
                    ' runserver 8000'
                   )],
    ]],
]
