# -*- coding: utf-8 -*-

# Production environment settings

import os
from .common import BASE_DIR, LOGGING


ALLOWED_HOSTS = ['95.142.174.235']
ENVIRONMENT = 'production'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.abspath(os.path.join(BASE_DIR, '../../tmp/databases', 'diapason_production.db'))
    }
}

REDIS = {
    'host': 'localhost',
    'port': 6379,
    'db': 2,
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'diapason_production',
        'TIMEOUT': 600,
    },

    'staticfiles': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'staticfiles',
        'TIMEOUT': 3600 * 24 * 8,
        'MAX_ENTRIES': 10000,
    },
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'

AVATAR_HOST = '95.142.174.235'

SITE_URL = 'http://95.142.174.235/'
AVATAR_URL = 'http://%s/avatar/' % AVATAR_HOST
STATIC_URL = 'http://95.142.174.235/static/'

SECRET_KEY = '.&Ci76G7xM9)ZBDuvm{{iRm8pUCF9EBM&bhY8sr%B;28+#C8oa'

STATICFILES_DIRS = ()

EMAIL_HOST = 'localhost'
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = ''
EMAIL_PORT = 25
EMAIL_USE_TLS = False

SOCIAL_AUTH_FACEBOOK_KEY = 'OBFUSCATED'
SOCIAL_AUTH_FACEBOOK_SECRET = 'OBFUSCATED'

LOGGING['loggers'] = {
    'django.request': {
        'handlers': ['django_file'],
        'level': 'ERROR',
        'propagate': True,
    },
    'django.security.DisallowedHost': {
        'handlers': ['null'],
        'propagate': False,
    },
}

DEPLOY = {
    'django': {
        'clean': 'find ./app/ -name "*.pyc" -exec rm -rf {} \;',
        'env': 'if [ ! -f "./env/bin/python" ]; then virtualenv -p /usr/bin/python2 env --no-site-packages; fi',
        'install': ['./tools/setup.py install', ['retry_error']],
        'syncdb': './tools/manage.py syncdb --noinput',
        'migrate': './tools/manage.py migrate --noinput',
        'collectstatic': './tools/manage.py collectstatic --noinput',
    },

    'static': {
        'install': 'cd ./static/; npm install',
        'clean': None, #'cd ./static/; npm run-script clean',
        'build': 'cd ./static/; npm run-script build-production',
    },

    'run': {
        'start': 'run web 95.142.174.235 start',
        'wait': True,
        'stop': 'run web 95.142.174.235 stop',
    }
}

RUN = [
    ['django', [
        ['server', (
                    './tools/manage.py'
                    ' runserver 8000'
                   )],
    ]],
]
