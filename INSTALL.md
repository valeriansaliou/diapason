Diapason Install Guide (English)
================================

**This file contains the installation instruction of the Diapason environment (development). The setup is similar for production environments.**

## Requirements Setup Guide

Some parts of this tutorial will require you to have Homebrew available (via the *brew* command).

Simply copy and paste this command to install it:

> ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"

If the command does not work, give a look to http://brew.sh/ to get latest installation instructions.

You may also need to install Xcode Command Line Tools (used for compiling latter sources):

> xcode-select --install

A prompt window will open, just follow the installation step.

## Django Setup Guide

This guide will help you get Diapason running on your local computer, thus so that you can develop on the top of it in good conditions.

This tutorial only covers MacOS systems with MAMP app. If your base system is different, give some Google searches and you'll find some good tuts.

### 1. Clone the Diapason code

In a terminal, go to your Web directory and execute the following command:

> git clone -b master git@github.com:valeriansaliou/diapason.git

Check you are currently using the master branch using:

> git branch

You should see something like: '* master'.

### 2. Configure MAMP

Now we need to properly configure MAMP to reproduce the final server conditions.

First of all, check that your MAMP environment **allows .htaccess files**, and that **mod_rewrite, mod_alias, mod_proxy and mod_proxy_http are enabled**.

Then, **create hosts**, pointing to the given folder (make the path absolute):

* **diapason.org.dev** - ./

Then, use the following MAMP host configuration:

```
location ~ ^/(static/(?!build).*) {
    rewrite  ^/static/(.*)$  /static/build/$1;
}

location ~ ^/((humans\.txt)|(favicon\.ico)|((tile|favicon(-\d+x\d+)?|apple-touch-icon(-\d+x\d+)?)\.png)) {
    rewrite  ^/(.*)  /public/$1;
}

location ~ "^/(?!(static/.*)|(humans\.txt)|(favicon\.ico)|((tile|favicon(-\d+x\d+)?|apple-touch-icon(-\d+x\d+)?)\.png))” {
  proxy_pass http://localhost:8000;

  proxy_set_header X-Real-IP '178.62.89.169';
  proxy_set_header Host $host;
  proxy_set_header X-Forwarded-For '178.62.89.169';
}
```

You may change the IP `178.62.89.169` to an IP of yourself. It is used to emulate a real Internet-wide client connecting to Diapason.

### 3. Install dependencies

Diapason requires a few external dependencies:

#### a. Install imaging libraries

Diapason use some advanced image manipulation techniques, which require you to setup JPEG and PNG libraries.

**Install them using Homebrew:**

> brew tap homebrew/dupes
> brew install libtiff libjpeg libpng zlib webp littlecms

### 4. Install Python stuff

Diapason is built on the top of Django, which must be installed on your system.

#### a. Install Python

Get Python 2.7 there: http://python.org/download/

Download the latest Python 2.7 MacOS 64 bits DMG file.

#### b. Install PIP

PIP is an excellent Python package manager, which allows you to seamlessly install any distributed Python module to your distribution, just as you would do with 'aptitude' on a Debian system.

We need it to install Django and further required Python packages. All in a snap.

Then, open a terminal and execute:

> curl http://python-distribute.org/distribute_setup.py | python2
> curl https://raw.github.com/pypa/pip/master/contrib/get-pip.py | python2
> export PATH=/Library/Frameworks/Python.framework/Versions/2.7/bin:$PATH

Last command, assuming your Python version number is 2.7 (latest of 2.x branch).

Check for errors, if none, PIP is now installed!

**Note: the path export we did above is not definitive. Once you'll start a new shell it will be lost if not written in .bash_profile - so remember to do so!**

#### c. Install Virtualenv

To install Virtualenv (that wraps a Python virtual environment for our dependencies to work cleanly, separately from other Python apps), execute this:

> sudo pip install virtualenv

### 5. Configure the Diapason environment

Before we can start Diapason, we just need to tell it we're going to use it in the development mode. Which we call 'development' environment.

#### a. Environment name

In a terminal, go to the Diapason Web folder, and then execute:

> cd app/_settings/
> touch environment.py
> nano environment.py

Paste the following code in the freshly created file:

```python
import os

os.environ["DJANGO_ENVIRONMENT"] = "development"
```

Then, save pressing CTRL + O.

**Note: we asked you to create that file because it is not (and it SHOULD NEVER BE) present on the Git remote.**

#### b. Environment settings

If ever, you need to add your own Django settings or override the default ones from your current environment, put them in the following file:

> app/settings/local.py

**Important: never change a predefined configuration file (like development.py) if you know the configuration lines that are changed are only for your needs. Remember that any change in there is made global across all developers.**


## Statics Setup Guide

Diapason uses smart tools to manage the way front-end developers work. This involves the use of SASS/Compass for stylesheets, as well as the GruntJS task automator for scripts (which is also used to call Compass and build everything in one command).

Let's install all that, step by step.

### 1. Install Compass

Compass is built upon the Ruby programming language. You first need to setup the Ruby interpreter and the Gems package manager.

#### a. Install Ruby

First, check that Ruby is not already installed on your system (it seems that on MacOS 10.8 it is already there, maybe that's just me).

Enter this in a terminal:

> ruby -v

If you don't get a line with the current Ruby version, you need to install Ruby, which you can get there: http://www.ruby-lang.org/en/downloads/

#### b. Install Gem

As with Ruby, Gem might be already there:

> gem -v

If not, go get it on: https://rubygems.org/pages/download

#### c. Install the Compass gem

Now that you got both Ruby and Gem to work, let's install Compass as a gem:

> sudo gem update --system
> sudo gem install compass

Test that Compass command can be called:

> compass -v

Nice, you got it running! Now, one last effort...

### 2. Install Grunt

Grunt is used to automate the whole process of static compilation (call to Compass, script minification/uglification and more).

It is written in JavaScript. Thus, it requires your system to have the NodeJS app.

#### a. Install NodeJS

Proceed the NodeJS setup:

> brew install node

Then test if both NodeJS and NPM are well installed:

> node -v
> npm -v

You shouldn't get any issue at this point. Otherwise, retry to install it or manually unlink/link it using Homebrew.

#### b. Install GruntJS

Now that you have both NodeJS and its package manager NPM available on your system, GruntJS can be installed in a snap:

> sudo npm install -g grunt-cli

Test the command now exists:

> grunt

You'll get an error from Grunt stating it could not find the Gruntfile. This is normal, it's telling us it's alive and working. That's good for now.

### 3. Install the build dependencies

Before you can start using Grunt to compile the whole static set, you have to initialize the NodeJS components (on which this Grunt project depends) in the static/ directory.

Move to the Diapason Web's static/ directory and execute:

> npm install

NPM (Node Package Manager) will start to fetch the dependencies (defined in package.json), that are used in the statics build process. It should take 1 minute or so.

**Note: if ever, you see that a change is done in the package.json file, or that GruntJS does not want to work anymore, remove the node_modules/ directory and re-proceed this point.**


## Let's Deploy!

All the work here is automated, lucky you. Simply execute the few commands listed below and you'll get Diapason up and running.

### 1. Deploy Diapason

Now that all the requirements are satisfied, we can deploy Diapason.

Execute the following command:

> ./tools/deploy.py

This will configure some random stuff, build static files, fetch all dependencies and install them. And finally: start Diapason!

**Note: when something is not working fine after you pulled from Git, the reason might be that a dependency has been added or updated. The no-brainer solution to this is to re-run the deploy command above, again.**

### 2. Start Diapason

Whenever you want to start again the Diapason services, run the following command:

> ./tools/run.py

All done, it's now back running!

### 3. Stop Diapason

Whenever you want to stop all the Diapason services, execute:

> ./tools/run.py terminate

You can also use the *kill* command below (which is faster to execute, but does not exit gracefully the services - which can result in some loss of data):

> ./tools/run.py kill

So far, so good. Choose the compromise that best suits your needs depending on the situation in which you use this set of commands.
