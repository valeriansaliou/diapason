__ = window



class Account
    init: ->
        try
            @_input_all = $ 'input'
            @_body = $ '#body'
        catch error
            Console.error 'Account.init', error


    focus_required_input: ->
        try
            @_body.find('input.input-error:visible, input:not([value]):visible').filter(':first').focus()
        catch error
            Console.error 'Account.focus_required_input', error



@Account = new Account



$(document).ready ->
    __.Account.init()
    __.Account.focus_required_input()
