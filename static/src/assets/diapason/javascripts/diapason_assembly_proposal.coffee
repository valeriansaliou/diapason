__ = window



class DiapasonAssemblyProposal
    init: ->
        try
            # Nothing done.
        catch error
            Console.error 'DiapasonAssemblyProposal.init', error


    submitVote: (elm) ->
        try
            $(elm).find('.assembly-sessions-contents-vote-choices-one-form').submit()
        catch error
            Console.error 'DiapasonAssemblyProposal.submitVote', error
        finally
            return false



@DiapasonAssemblyProposal = new DiapasonAssemblyProposal



$(document).ready ->
    __.DiapasonAssemblyProposal.init()
