__ = window



class DiapasonNew
    init: ->
        try
            # Bind events
            @_events()
        catch error
            Console.error 'DiapasonNew.init', error


    reprocessMemberIds: () ->
        try
            member_usernames = []

            $('.ticked-user:not(.ticked-user-template)').each((_, user_sel) ->
                member_usernames.push $(user_sel).attr('data-username')
            )

            $('.invite-search-member-ids').val(member_usernames.join('|'))
        catch error
            Console.error 'DiapasonNew.reprocessMemberIds', error


    nukeUser: (elm) ->
        try
            $(elm).remove()

            @reprocessMemberIds()
        catch error
            Console.error 'DiapasonNew.nukeUser', error
        finally
            return false


    nukeCycle: (elm) ->
        try
            $(elm).parents('label.cycle-one').remove()
        catch error
            Console.error 'DiapasonNew.nukeCycle', error
        finally
            return false


    addCycle: (elm) ->
        try
            template_sel = $('.cycle-one-template').clone()
            template_sel.removeClass 'cycle-one-template'

            count = $('.cycle-one').size()

            template_sel.find('.cycle-count').text count

            template_sel.find('.cycle-defined').attr 'name', "cycle_#{count}_defined"
            template_sel.find('.cycle-name').attr 'name', "cycle_#{count}_name"
            template_sel.find('.cycle-tags').attr 'name', "cycle_#{count}_tags"
            template_sel.find('.cycle-topic').attr 'name', "cycle_#{count}_topic"
            template_sel.find('.cycle-autoclose').attr 'name', "cycle_#{count}_autoclose"

            template_sel.appendTo '.cycles-wrap'
        catch error
            Console.error 'DiapasonNew.addCycle', error
        finally
            return false


    addTicked: (username, full_name) ->
        try
            @nukeSearch true

            if $(".ticked-user[data-username='#{username}']").size() == 0
                template_sel = $('.ticked-user-template').clone()
                template_sel.removeClass 'ticked-user-template'

                template_sel.find('.user-name').text full_name
                template_sel.attr 'data-username', username

                template_sel.appendTo '.search-ticked'

                @reprocessMemberIds()
        catch error
            Console.error 'DiapasonNew.addTicked', error
        finally
            return false


    nukeSearch: (reset) ->
        try
            $('.invite-search-invite').hide()
            $('.invite-search-invite-item:not(.invite-search-invite-item-template)').remove()

            if reset == true
                $('.invite-search-box').val('').focus()
        catch error
            Console.error 'DiapasonNew.nukeSearch', error
        finally
            return false


    addSearch: (username, full_name) ->
        try
            template_sel = $('.invite-search-invite-item-template').clone()
            template_sel.removeClass 'invite-search-invite-item-template'

            template_sel.find('.invite-search-invite-item-link-name').text full_name
            template_sel.attr 'data-username', username
            template_sel.attr 'onclick', "return DiapasonNew.addTicked('#{username}', '#{full_name}')"

            template_sel.appendTo '.invite-search-invite'

            $('.invite-search-invite').show()
        catch error
            Console.error 'DiapasonNew.addSearch', error
        finally
            return false


    _proceedSearch: (elm) ->
        try
            self = @

            @nukeSearch()

            searchQuery = $(elm).val()

            if /([^@]+)@([^@]+)\.([^@]+)/.test(searchQuery) == true
                self.addSearch searchQuery, searchQuery
            else
                $.getJSON("/diapason/new/user/search/#{searchQuery}/", (results) ->
                    results.forEach((result) ->
                        self.addSearch result.fields.username, "#{result.fields.first_name} #{result.fields.last_name}"
                    )
                )
        catch error
            Console.error 'DiapasonNew._proceedSearch', error


    _events: () ->
        try
            self = @

            $('li.visibility-btn').click ->
                $('li.visibility-btn').removeAttr 'data-active'
                $(this).attr 'data-active', 'true'

                $('input[name="visibility_level"]').val(
                    $(this).attr('data-value')
                )

                false

            $('input.invite-search-box').keyup ->
                self._proceedSearch this
        catch error
            Console.error 'DiapasonNew._events', error



@DiapasonNew = new DiapasonNew



$(document).ready ->
    __.DiapasonNew.init()
