__ = window



class DiapasonAssembly
    init: ->
        try
            # Bind events
            @_events()
        catch error
            Console.error 'DiapasonAssembly.init', error


    newAdd: (elm) ->
        try
            $(elm).hide()

            $(".assembly-sessions-current-add").addClass(
                "assembly-sessions-current-add-opened"
            )

            $(".assembly-sessions-current-add-form").slideDown(
              () ->
                $(".assembly-sessions-current-add-form-fields-input").focus()
            )
        catch error
            Console.error 'DiapasonAssembly.newAdd', error
        finally
            return false


    addService: (elm) ->
        try
            $(elm).hide()

            $(".assembly-sessions-discuss-add-form").slideDown()
        catch error
            Console.error 'DiapasonAssembly.addService', error
        finally
            return false


    designateDelegate: (elm) ->
        try
            $(elm).hide()

            $(".assembly-sessions-current-delegate").addClass(
                "assembly-sessions-current-invite-opened"
            )

            $(".assembly-sessions-current-delegate-form").slideDown(
              () ->
                $(".assembly-sessions-current-delegate-form-fields-input:first").focus()
            )
        catch error
            Console.error 'DiapasonAssembly.designateDelegate', error
        finally
            return false


    undesignateDelegate: () ->
        try
            $('.assembly-sessions-current-undelegate-form').submit()
        catch error
            Console.error 'DiapasonAssembly.undesignateDelegate', error
        finally
            return false


    inviteMember: (elm) ->
        try
            $(elm).hide()

            $(".assembly-sessions-current-invite").addClass(
                "assembly-sessions-current-invite-opened"
            )

            $(".assembly-sessions-current-invite-form").slideDown(
              () ->
                $(".assembly-sessions-current-invite-form-fields-input:first").focus()
            )
        catch error
            Console.error 'DiapasonAssembly.inviteMember', error
        finally
            return false


    nukeSearch: (path, reset) ->
        try
            $('.' + path + '-box').hide()
            $('.' + path + '-box-item:not(.' + path + '-box-item-template)').remove()

            if reset == true and path == 'assembly-sessions-current-invite'
                $('.' + path + '-form-fields-input').val('').focus()
        catch error
            Console.error 'DiapasonAssembly.nukeSearch', error
        finally
            return false


    nukeDelegation: (id, subject) ->
        try
            $('input[name="undelegate_member_id"]').val id
            $('input[name="undelegate_subject"]').val subject

            $('form.assembly-sessions-current-delegate-form-nuke').submit()
        catch error
            Console.error 'DiapasonAssembly.nukeDelegation', error
        finally
            return false


    addTicked: (path, username, full_name) ->
        try
            @nukeSearch path, true

            $('.' + path + '-form-fields-member-ids').val(username)
            $('.' + path + '-form').submit()
        catch error
            Console.error 'DiapasonAssembly.addTicked', error
        finally
            return false


    addSearch: (path, username, full_name) ->
        try
            template_sel = $('.' + path + '-box-item-template').clone()
            template_sel.removeClass(path + '-box-item-template')

            template_sel.find('.' + path + '-box-item-link-name').text full_name
            template_sel.attr 'data-username', username
            template_sel.attr 'onclick', "return DiapasonAssembly.addTicked('#{path}', '#{username}', '#{full_name}')"

            template_sel.appendTo('.' + path + '-box')

            $('.' + path + '-box').show()
        catch error
            Console.error 'DiapasonAssembly.addSearch', error
        finally
            return false


    _proceedSearch: (path, elm) ->
        try
            self = @

            @nukeSearch path

            searchQuery = $(elm).val()

            if /([^@]+)@([^@]+)\.([^@]+)/.test(searchQuery) == true
                self.addSearch path, searchQuery, searchQuery
            else
                $.getJSON("/diapason/new/user/search/#{searchQuery}/", (results) ->
                    results.forEach((result) ->
                        self.addSearch path, result.fields.username, "#{result.fields.first_name} #{result.fields.last_name}"
                    )
                )
        catch error
            Console.error 'DiapasonAssembly._proceedSearch', error


    _events: () ->
        try
            self = @

            $('li.visibility-btn').click ->
                $('li.visibility-btn').removeAttr 'data-active'
                $(this).attr 'data-active', 'true'

                $('input[name="visibility_level"]').val(
                    $(this).attr('data-value')
                )

                false

            $('input.assembly-sessions-current-invite-form-fields-input').keyup ->
                self._proceedSearch 'assembly-sessions-current-invite', this

            $('input.assembly-sessions-current-delegate-form-fields-input[name="delegate_name"]').keyup ->
                self._proceedSearch 'assembly-sessions-current-delegate', this
        catch error
            Console.error 'DiapasonAssembly._events', error



@DiapasonAssembly = new DiapasonAssembly



$(document).ready ->
    __.DiapasonAssembly.init()
