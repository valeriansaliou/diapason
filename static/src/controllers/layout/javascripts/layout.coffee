__ = window


class LayoutHeader
    init: ->
        try
            # Selectors
            @_body_sel = $ 'body'
            @_type_box_sel = $ '#typeBox'
            @_type_box_li_sel = @_type_box_sel.find 'li'
            @_type_input_sel = $ '#typeInput'
            @_result_type_sel = $ '#resultType'
            @_suggest_pane_sel = $ '#suggestPane'
            @_search_engine_form_sel = $ 'form.search-engine-form'
            @_user_profile_sel = $ '.user-profile-header .profile'
            @_user_settings_sel = $ '#userSettings .arrow-deploy'
            @_user_settings_deploy_sel = $ '#userSettingsDeploy'
            @_search_mobile_tablet_sel = $ '#searchMobile, #searchTablet'
            @_search_mobile_tablet_btn_sel = $ '#searchMobileButton, #searchTabletButton'
            @_quick_sign_sel = $ '#quickSignBox'
            @_quick_sign_form_sel = @_quick_sign_sel.find '.qs-form'
            @_quick_sign_buttons_sel = @_quick_sign_sel.find '.qs-buttons'

            # States
            @_is_writing_opened = false
            @_is_suggest_pane_opened = false
            @_is_user_settings_opened = false

            # Constants
            @_suggest_pane_action = @_suggest_pane_sel.attr 'data-action'
        catch error
            Console.error 'LayoutHeader.init', error


    events: ->
        try
            @event_toggle_mobile_search()
            @event_deploy_result_type()
            @event_update_type_box()
            @event_search_form()
            @event_toggle_user_settings()
            @event_toggle_quick_signin()
        catch error
            Console.error 'LayoutHeader.events', error


    _open_view: (status_bool, open_fn, close_fn) ->
        try
            if status_bool is false
                if @_has_view_open()
                    @_close_all()

                (open_fn.bind @)()
            else
                (close_fn.bind @)()
        catch error
            Console.error 'LayoutHeader._open_view', error


    _close_all: ->
        try
            if @_is_user_settings_opened is true
                @_close_user_settings()
        catch error
            Console.error 'LayoutHeader._close_all', error


    _unbind_all: ->
        try
            @_body_sel.off 'click'
        catch error
            Console.error 'LayoutHeader._unbind_all', error


    _has_view_open: ->
        try
            return @_is_writing_opened or
                   @_is_user_settings_opened
        catch error
            Console.error 'LayoutHeader._has_view_open', error


    _toggle_mobile_search: ->
        try
            @_search_mobile_tablet_sel.stop(true).slideToggle()
        catch error
            Console.error 'LayoutHeader._toggle_mobile_search', error


    event_toggle_mobile_search: ->
        try
            self = @

            @_search_mobile_tablet_btn_sel.on(
                'click',
                ->
                    self._toggle_mobile_search()
                    return false
            )
        catch error
            Console.error 'LayoutHeader.event_toggle_mobile_search', error


    _deploy_result_type: ->
        try
            @_type_box_sel.fadeIn(100).addClass 'active'
            @_bind_close_result_type()
        catch error
            Console.error 'LayoutHeader._deploy_result_type', error


    event_deploy_result_type: ->
        try
            self = @

            @_result_type_sel.on(
                'click',
                ->
                    self._deploy_result_type()
                    return false
            )
        catch error
            Console.error 'LayoutHeader.event_deploy_result_type', error


    _bind_close_result_type: ->
        try
            self = @

            @_body_sel.on(
                'click',
                (evt) ->
                    try
                        tg = $ evt.target

                        if not (tg.parents('#resultType').size() or
                                (tg.is '#resultType') or
                                tg.parents('#typeBox').size() or
                                (tg.is '#typeBox'))
                            self._close_type_box()
                    catch _error
                        Console.error 'LayoutHeader._bind_close_result_type[event:click]', _error
            )
        catch error
            Console.error 'LayoutHeader._bind_close_result_type', error


    _unbind_close_result_type: ->
        try
            @_unbind_all()
        catch error
            Console.error 'LayoutHeader._unbind_close_result_type', error


    _update_type_box: (element_sel) ->
        try
            # Read picked type
            type_id = element_sel.data 'type'
            type_value = element_sel.text()

            # Apply new type
            @_result_type_sel.text type_value
            @_type_box_sel.children().prepend element_sel

            @_type_input_sel.val type_id
            @_close_type_box()

            # Update suggest pane
            if @_is_suggest_pane_opened
                @_search_suggest_pane()
        catch error
            Console.error 'LayoutHeader._update_type_box', error


    _close_type_box: ->
        try
            @_type_box_sel.fadeOut(100).removeClass 'active'
            @_unbind_close_result_type()
        catch error
            Console.error 'LayoutHeader._close_type_box', error


    event_update_type_box: ->
        try
            self = @

            @_type_box_li_sel.on(
                'click',
                ->
                    self._update_type_box $(this)
                    return false
            )
        catch error
            Console.error 'LayoutHeader.event_update_type_box', error


    event_search_form: ->
        try
            self = @

            @_search_engine_form_sel.submit ->
                # Virtually submit form (handles async page load)
                form_sel = $ this
                form_action = form_sel.attr 'action'

                q_val = form_sel.find('input[name="q"]').val()
                t_val = form_sel.find('input[name="t"]').val()

                self._close_suggest_pane()
                self._prevent_sticky_suggest_pane()

                return false
        catch error
            Console.error 'LayoutHeader.event_search_form', error


    _open_user_settings: ->
        try
            @_is_user_settings_opened = true
            @_user_settings_sel.addClass 'active'
            @_user_settings_deploy_sel.stop(true).show()
        catch error
            Console.error 'LayoutHeader._open_user_settings', error
        finally
            return true


    _close_user_settings: ->
        try
            @_is_user_settings_opened = false
            @_user_settings_sel.removeClass 'active'
            @_user_settings_deploy_sel.stop(true).hide()
        catch error
            Console.error 'LayoutHeader._close_user_settings', error
        finally
            return true


    _toggle_user_settings: ->
        try
            return @_open_view(
                @_is_user_settings_opened,
                @_open_user_settings,
                @_close_user_settings
            )
        catch error
            Console.error 'LayoutHeader._toggle_user_settings', error


    event_toggle_user_settings: ->
        try
            self = @

            @_user_settings_sel.mouseenter ->
                if not self._is_user_settings_opened
                    self._toggle_user_settings()
                return false

            @_user_profile_sel.mouseleave ->
                self._close_user_settings()
                return false
        catch error
            Console.error 'LayoutHeader.event_toggle_user_settings', error


    event_toggle_quick_signin: ->
        try
            self = @

            @_quick_sign_buttons_sel.find('a.qs-login').on(
                'click',
                ->
                    try
                        self._quick_sign_buttons_sel.fadeOut(
                            250,
                            ->
                                self._quick_sign_form_sel.fadeIn(
                                    250,
                                    -> $(this).find('input.qs-email').focus()
                                )
                        )
                    catch _error
                        Console.error 'LayoutHeader.event_toggle_quick_signin[async]', _error
                    finally
                        return false
            )

            @_quick_sign_form_sel.find('button.qs-cancel').on(
                'click',
                ->
                    try
                        self._quick_sign_form_sel.find('input.qs-email').blur()

                        self._quick_sign_form_sel.fadeOut(
                            250,
                            -> self._quick_sign_buttons_sel.fadeIn 250
                        )
                    catch _error
                        Console.error 'LayoutHeader.event_toggle_quick_signin[async]', _error
                    finally
                        return false
            )
        catch error
            Console.error 'LayoutHeader.event_toggle_quick_signin', error


class LayoutMisc
    init: ->
        try
            # Selectors
            @_window = $ window
            @_growl_notif = $ '.growl-notification'
            @_growl_notif_close = @_growl_notif.find '.close'

            # Values
            @_clipboard_flash = ($('html').attr 'data-url-static') + 'vendor/jquery/flashes/jquery.clipboard.swf'
            @_window_width = @_window.width()
            @_desktop_wide_width = 1200
            @_desktop_normal_width = 1024
            @_tablet_large_width = 768
            @_tablet_small_width = 600
            @_mobile_width = 320
            @_has_window_focus = true
            @_key_event = {}
        catch error
            Console.error 'LayoutMisc.init', error


    events: ->
        try
            # Remove Facebook Connect URL fragment
            @remove_facebook_connect_hash()
        catch error
            Console.error 'LayoutMisc.events', error


    has_focus_window: ->
        has_focus = @_has_window_focus

        try
            has_focus = document.hasFocus() and true
        catch error
            Console.error 'LayoutMisc.has_focus_window', error
        finally
            return has_focus


    event_focus_window: ->
        try
            self = @

            @_window.focus -> (self._has_window_focus = true)

            @_window.blur -> (self._has_window_focus = false)
        catch error
            Console.error 'LayoutMisc.event_focus_window', error


    remove_facebook_connect_hash: ->
        try
            # There is no way to get rid of this just after user logins, except using this...
            if window.location.hash and window.location.hash is '#_=_'
                try
                    window.history.pushState(
                        '',
                        document.title,
                        "#{window.location.pathname}#{window.location.search}"
                    )
                catch _error
                    window.location.hash = ''
        catch error
            Console.error 'LayoutMisc.remove_facebook_connect_hash', error


    apply_clipboard: (copy_sel, callback) ->
        try
            self = @

            if typeof copy_sel isnt 'object'
                Console.error 'LayoutMisc.apply_clipboard', 'You need to provide a valid jQuery selector'
                return

            if typeof callback isnt 'function'
                Console.error 'LayoutMisc.apply_clipboard', 'You need to provide a valid callback function'
                return

            copy_sel.on(
                'click',
                (e) -> e.preventDefault()
            )

            copy_sel.clipboard(
                path: self._clipboard_flash,
                copy: callback
            )
        catch error
            Console.error 'LayoutMisc.apply_clipboard', error


    event_clipboard_code: (parent) ->
        try
            self = @

            @apply_clipboard(
                ($('.block-code a.code-copy').hasParent parent),
                ->
                    this_sel = $ this

                    this_sel.find('.code-copy-first').hide()
                    this_sel.find('.code-copy-done').show()

                    return self._clipboard_code this_sel.closest '.block-code'
            )
        catch error
            Console.error 'LayoutMisc.event_clipboard_code', error


    _clipboard_code: (code_block_sel) ->
        try
            code_text = ''

            # Prepend copyright information
            #code_text += @_clipboard_copyright code_block_sel

            # Append lines of code (reformatted)
            code_block_sel.find('.code-line').each ->
                if code_text
                    code_text += '\n'

                code_text += $(this).text()

            return code_text
        catch error
            Console.error 'LayoutMisc._clipboard_code', error


    _clipboard_copyright: (code_block_sel) ->
        try
            code_text = ''
            code_copyright = code_block_sel.attr 'data-copyright'
            code_language = code_block_sel.attr 'data-language'

            if code_copyright and code_language
                code_copyright = _.str.sprintf code_copyright, window.location.href

                switch code_block_sel.attr 'data-language'
                    when 'html', 'xml'
                        code_text += "<!-- #{code_copyright} -->"
                    when 'php', 'js', 'css', 'sass', 'scss'
                        code_text += "/* #{code_copyright} */"
                    else
                        code_text += "##{code_copyright}"

                code_text += '\n'

            return code_text
        catch error
            Console.error 'LayoutMisc._clipboard_copyright', error


    authenticate_tooltip: (action_sel, redirect_data) ->
        try
            login_fn = ->
                if redirect_data.login
                    document.location.href = redirect_data.login

            register_fn = ->
                if redirect_data.register
                    document.location.href = redirect_data.register

            # Create tooltip
            new Tooltip(
                action_sel,
                (action_sel.attr 'data-tooltip'),

                [
                    [
                        'Sign In',
                        'info',
                        login_fn
                    ],

                    [
                        'Sign Up',
                        'primary',
                        register_fn
                    ]
                ]
            )
        catch error
            Console.error 'LayoutMisc.authenticate_tooltip', error


    events_key: ->
        try
            self = @

            @_window.keydown (evt) -> (self._key_event = evt)

            @_window.keyup -> (self._key_event = {})
        catch error
            Console.error 'LayoutMisc.events_key', error


    key_event: ->
        try
            return @_key_event
        catch error
            Console.error 'LayoutMisc.key_event', error


    is_desktop: ->
        try
            return @is_normal_desktop() or @is_wide_desktop()
        catch error
            Console.error 'LayoutMisc.is_desktop', error


    is_wide_desktop: ->
        try
            return @_window_width >= @_desktop_wide_width
        catch error
            Console.error 'LayoutMisc.is_wide_desktop', error


    is_normal_desktop: ->
        try
            return @_window_width >= @_desktop_normal_width and
                   @_window_width < @_desktop_wide_width
        catch error
            Console.error 'LayoutMisc.is_normal_desktop', error


    is_tablet: ->
        try
            return @is_small_tablet() or @is_large_tablet()
        catch error
            Console.error 'LayoutMisc.is_tablet', error


    is_large_tablet: ->
        try
            return @_window_width >= @_tablet_large_width and
                   @_window_width < @_desktop_normal_width
        catch error
            Console.error 'LayoutMisc.is_large_tablet', error


    is_small_tablet: ->
        try
            return @_window_width >= @_tablet_small_width and
                   @_window_width < @_tablet_large_width
        catch error
            Console.error 'LayoutMisc.is_small_tablet', error


    is_mobile: ->
        try
            return @_window_width >= @_mobile_width and
                   @_window_width < @_tablet_small_width
        catch error
            Console.error 'LayoutMisc.is_mobile', error



@LayoutHeader           = new LayoutHeader
@LayoutMisc             = new LayoutMisc



$(document).ready ->
    # Initialize controllers
    __.LayoutHeader.init()
    __.LayoutMisc.init()

    # Watch for events
    __.LayoutHeader.events()
    __.LayoutMisc.events()
