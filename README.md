Diapason
========

Decision-making interface for groups, built in Django / Python.

ENSSAT CS project (2016).

# Voting Results

The voting results are calculated according to the Schulze method, which is a Condorcet method.

The results are computed using a Schulze ballot implementation in Python: [https://pypi.python.org/pypi/python-vote-core/20120423.0](https://pypi.python.org/pypi/python-vote-core/20120423.0)

The Schulze method applies as such:

1. People vote in order of preference (they can ignore choices, resulting in preferring all ranked choices over non-ranked choices).
2. People votes are aggregated, and the system groups similar (exact) vote orders and count each vote variant occurence.
3. The aggregated results are ranked according to the Schulze method. We get the preferred result among all votes, or an ordered list of preferred results in case multiple results win.
